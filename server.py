import socket
 
HOST = 'angsila.informatics.buu.ac.th'  
PORT = 5432         
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind((HOST, PORT))

s.listen()
 
while True:
    
    print("waiting for connection")
    
    connection, client_address = s.accept()
    try:
        print("connection from", client_address)

        a = open('a.txt', 'r') 
        hello = a.read()
        a.close()

        connection.sendall(hello.encode())
            
    finally:
        connection.close()
        print("closed connection")
        break